import 'bootstrap';
import DropZone from 'dropzone';

import '../css/dropzone.css';

DropZone.autoDiscover = false;

const dictMaxFilesExceeded = 'You can only upload 1 file at a time';
const dictDefaultMessage = 'Drop a .csv file here to upload';
const errorOneFile = 'You can only upload one file at a time';

const url = ($('#products-dropzone').data('url'));

let dropZone = new DropZone('#products-dropzone', {
    paramName: 'file',
    maxFiles: 1, // limits uploads to a single file
    url,
    dictMaxFilesExceeded,
    autoProcessQueue: false, // disables auto uploading
    dictDefaultMessage,
    acceptedFiles: 'text/csv',
    accept: function(file, done) {
        // remove progress bar on selected files
        file.previewElement.querySelector(".dz-progress").remove();
        done();
    },
    init: function () {
        this.on('maxfilesexceeded', function(file){
            this.emit('error', file, errorOneFile);
        });
        this.on('error', function (file, data) {
            if(data.detail) {
                this.emit('error', file, data.detail);
            }
        });
    }
});

$('#product_upload_submit').click(function (e) {
    e.preventDefault();
    $('#errors-body').empty();
    $('#report-log').hide();
    dropZone.processQueue(); // uploads file only when submit button is pressed
    dropZone.on('queuecomplete', function (file) {
        setTimeout(function () {
            dropZone.removeAllFiles();
        }, 5000);
    });
    dropZone.on('success', function (file, response) {
        $('#errors-body').empty();
        if (response.errors.length) {
            response.errors.forEach((error) => {
                $('#errors-body').append(addError(error));
            });
        }

        let processed = response.processed;
        let succeed = response.products.length;
        let skipped = response.skipped;
        $('#report-log').text('Processed : ' + processed + ' , Successful : ' + succeed  + ' , Skipped : ' + skipped);
        $('#report-log').show();
    });
});

const addError = (error) => {
    let errorRow = document.createElement('tr');
    let errorData = document.createElement('td');
    errorData.textContent = error;
    errorRow.appendChild(errorData);

    return errorRow;
};
