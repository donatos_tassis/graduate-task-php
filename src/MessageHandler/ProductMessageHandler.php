<?php

namespace App\MessageHandler;

use App\Message\ProductMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductMessageHandler implements MessageHandlerInterface
{
    /** @var EntityManagerInterface $entityManager*/
    private $entityManager;

    /**
     * ProductMessageHandler constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProductMessage $productMessage)
    {
        $product = $productMessage->getProduct();

        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}
