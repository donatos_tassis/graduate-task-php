<?php

namespace App\Twig;

use DateTime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DateTimeExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isDateTime', [$this, 'isDateTime']),
            new TwigFunction('dateString', [$this, 'dateToString']),
        ];
    }

    public function isDateTime($value)
    {
        return $value instanceof DateTime;
    }

    public function dateToString($value)
    {
        return date_format($value,'Y-m-d H:i:s');
    }
}
