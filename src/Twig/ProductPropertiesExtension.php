<?php

namespace App\Twig;

use App\Entity\Product;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ProductPropertiesExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('productFields', [$this, 'getProductFields']),
            new TwigFunction('displayFields', [$this, 'formatDisplayFields']),
        ];
    }

    public function getProductFields($product)
    {
        if($product instanceof Product) {
            return $product->getProperties();
        }
    }

    public function formatDisplayFields($field)
    {
        // convert camel case to space separated -> e.g.: camelCase  ---> camel Case
        $property = preg_replace('/([a-z])([A-Z])/s','$1 $2', $field);

        return ucfirst($property);
    }
}
