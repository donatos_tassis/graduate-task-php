<?php

namespace App\Services;

use App\Validator\FileValidatable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class FileUploadHandler
 *
 * @package App\Services
 */
class FileUploadHandler
{
    /** @var FileValidatable $validator */
    private $validator;

    /** @var bool */
    private $validated = true;

    /**
     * FileUploadHandler constructor.
     *
     * @param FileValidatable $validator
     */
    public function __construct(FileValidatable $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated;
    }

    /**
     * @param File $file
     *
     * @return string
     */
    public function getContents(File $file)
    {
        return $this->removeEmptyLines($file);
    }

    /**
     * Returns an array of validation errors for the given file if any found
     *
     * @param File $file
     *
     * @return array
     */
    public function getValidationErrors(File $file)
    {
        /** @var ConstraintViolationList $violations */
        $violations = $this->validator->validate($file);

        $errors = [];
        foreach ($violations as $violation) {
            $errors[] = $violation->getMessage();
        }

        if(count($errors) > 0)
            $this->validated = false;

        return $errors;
    }

    /**
     * Removes empty lines from csv contents
     *
     * @param File $file
     *
     * @return string
     */
    private function removeEmptyLines(File $file)
    {
        $contents = '';
        $rows = file($file);

        foreach ($rows as $row) {
            if(substr($row, 0, 2) == PHP_EOL ) {
                continue;
            }

            $contents .= $row;
        }

        return $contents;
    }
}
