<?php

namespace App\Services;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonResponseBuilder
 *
 * @package App\Services
 */
class JsonResponseBuilder extends JsonResponse
{
    /** @var array $errors */
    private $errors = [];

    /** @var array $products */
    private $products = [];

    /**
     * JsonResponseBuilder constructor.
     *
     * @param array $data
     * @param int $status
     * @param array $headers
     * @param bool $json
     */

    public function __construct(array $data = [], int $status = 200 ,array $headers = [], bool $json = false)
    {
        $this->addErrors($data['errors']);
        $this->addProducts($data['succeed']);

        $data = [
            'errors' => $this->getErrors(),
            'products' =>$this->getProducts(),
            'skipped' => $data['skipped'],
            'processed' => $data['total']
        ];

        parent::__construct($data, $status);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function addErrors($errors): void
    {
        foreach ($errors as $error) {
            $this->errors[] = $error;
        }
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function addProducts(array $products): void
    {
        /** @var Product $product */
        foreach ($products as $product) {
            $this->products[] = $product->getCode();
        }
    }
}
