<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;
use App\Message\MessageDispatcher;
use App\Serializer\Model\ProductData;
use App\Serializer\Model\SucceedProductCollection;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductParser
 *
 * @package App\Services
 */
class Parser
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var MessageDispatcher */
    private $dispatcher;

    /** @var NormalizerInterface */
    private $normalizer;

    /** @var SerializerInterface */
    private $serializer;

    /**
     * Parser constructor.
     * @param ValidatorInterface $validator
     * @param MessageDispatcher $dispatcher
     * @param NormalizerInterface $normalizer
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ValidatorInterface $validator,
        MessageDispatcher $dispatcher,
        NormalizerInterface $normalizer,
        SerializerInterface $serializer
    ) {
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->normalizer = $normalizer;
        $this->serializer = $serializer;
    }

    /**
     * @param string $contents the csv file contents
     *
     * @return array
     */
    public function parse(string $contents)
    {
        $products = $this->serializer->decode($contents, 'csv');

        $errors = [];
        $succeed = [];
        foreach ($products as $product) {
            $productData = new ProductData($product);
            $violations = $this->validator->validate($productData);
            if(count($violations) > 0) {
                foreach ($violations as $violation) {
                    $errors[] = $violation->getMessage();
                }
                continue;
            }

            $normalizedProduct = $this->normalizer->denormalize($product, Product::class);

            // if $normalizedProduct is an array means that it is an array of errors
            if(is_array($normalizedProduct)) {
                foreach($normalizedProduct as $error) {
                    $errors[] = $error;
                }
                continue;
            }
            $succeed[] = $normalizedProduct;
        }

        $duplicated = $this->validator->validate(new SucceedProductCollection($succeed));
        $modulator = 0;
        foreach ($duplicated as $duplicate) {
            $errors[] = $duplicate->getMessage();
            $parameters = $duplicate->getParameters();
            $index = $parameters['index'] - $modulator;
            array_splice($succeed, $index, 1);
            $modulator++;
        }

        $this->dispatcher->dispatch($succeed);

        return [
            'errors' => $errors,
            'skipped' => count($products) - count($succeed),
            'succeed' => $succeed,
            'total' => count($products)
        ];
    }
}
