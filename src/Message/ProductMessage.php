<?php

namespace App\Message;

use App\Entity\Product;

class ProductMessage
{
    /** @var Product $product */
    private $product;

    /**
     * ProductMessage constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}
