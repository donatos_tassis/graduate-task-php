<?php

namespace App\Message;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class MessageDispatcher
{
    /** @var MessageBusInterface $messageBus */
    private $messageBus;

    /**
     * MessageDispatcher constructor.
     * @param MessageBusInterface $messageBus
     */
    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @param array $succeedProducts
     */
    public function dispatch(array $succeedProducts)
    {
        foreach ($succeedProducts as $product) {
            $message = new ProductMessage($product);
            $envelope = new Envelope($message);
            $this->messageBus->dispatch($envelope);
        }
    }
}
