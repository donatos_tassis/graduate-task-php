<?php

namespace App\Validator;

use App\Serializer\Model\SucceedProductCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueProductCodeValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @inheritDoc
     */
    public function validate($succeed, Constraint $constraint)
    {
        /* @var $constraint UniqueProductCode */

        if (null === $succeed || '' === $succeed) {
            return;
        }

        $duplicateProducts = $this->getDuplicateProducts($succeed);
        $this->addCodeInListViolation($duplicateProducts, $constraint);

        $inDatabaseProducts = $this->getInDatabaseProducts($succeed);
        $this->addCodeInDatabaseViolation($inDatabaseProducts, $constraint);
    }

    /**
     * @param SucceedProductCollection $succeed
     *
     * @return Product[]
     */
    private function getDuplicateProducts(SucceedProductCollection $succeed)
    {
        $duplicateProducts = [];
        $existedProducts = [];
        $index = 0;
        /** @var \App\Entity\Product $product */
        foreach ($succeed->getSucceed() as $product) {
            $code = $product->getCode();

            if(in_array($code, $existedProducts)) {
                if(!in_array($code, $duplicateProducts)) {
                    $duplicateProducts[] = [
                        'index' => $index,
                        'product' => $product
                    ];
                }
            } else {
                $existedProducts[] = $code;
            }
            $index++;
        }

        return $duplicateProducts;
    }

    /**
     * @param SucceedProductCollection $succeed
     *
     * @return Product[]
     */
    private function getInDatabaseProducts(SucceedProductCollection $succeed)
    {
        $inDatabaseProducts = [];

        /** @var \App\Entity\Product $product */
        foreach ($succeed->getSucceed() as $product) {
            $existedProduct = $this->manager->getRepository(\App\Entity\Product::class)
                ->findOneBy(['code' => $product->getCode()]);
            if($existedProduct) {
                $inDatabaseProducts[] = $existedProduct;
            }
        }

        return $inDatabaseProducts;
    }

    /**
     * @param array $repeatedProducts
     * @param UniqueProductCode $constraint
     */
    private function addCodeInListViolation(array $repeatedProducts, UniqueProductCode $constraint)
    {
        foreach ($repeatedProducts as $product) {
            $this->context->buildViolation($constraint->codeInList)
                ->setParameter('{{ code }}', $product['product']->getCode())
                ->setParameter('index', $product['index'])
                ->addViolation();
        }
    }

    /**
     * @param array $inDatabaseProducts
     * @param UniqueProductCode $constraint
     */
    private function addCodeInDatabaseViolation(array $inDatabaseProducts, UniqueProductCode $constraint)
    {
        foreach ($inDatabaseProducts as $index => $product) {
            $this->context->buildViolation($constraint->codeInDatabase)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('index', $index)
                ->addViolation();
        }
    }
}
