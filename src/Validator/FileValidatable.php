<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Interface FileValidatable
 *
 * @package App\Validator
 */
interface FileValidatable
{
    public function validate(File $file);
}