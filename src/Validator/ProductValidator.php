<?php

namespace App\Validator;

use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Entity\Product as EntityProduct;

/**
 * Class ProductValidator
 * @package App\Validator
 */
class ProductValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ProductValidator constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function validate($product, Constraint $constraint)
    {
        /* @var $constraint Product */
        if (null === $product || '' === $product) {
            return;
        }

        $this->addStockValidation($product, $constraint);
        $this->addNetCostValidation($product, $constraint);

        $properties = $this->getProperties($product);

        /** @var ReflectionProperty $property */
        foreach ($properties as $property) {

           if(in_array($property->getName(), Product::EXCLUDED_KEYS)) {
               continue;
           }

            // set the property's accessibility to true to access the value
            $property->setAccessible(true);

            $this->addPropertyNullValidation($product, $constraint, $property);
            $this->addPropertyEmptyValidation($product, $constraint, $property);
        }
    }

    /**
     * Adds validation for minimum value of Stock
     *
     * @param EntityProduct $product
     * @param Constraint $constraint
     */
    private function addStockValidation(EntityProduct $product, Constraint $constraint) : void
    {
        if ($product->getStock() < Product::STOCK_MIN) {
            $this->context->buildViolation($constraint->stockMinMessage)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('{{ stockMin }}', Product::STOCK_MIN)
                ->addViolation();
        }
    }

    /**
     * Adds validation for minimum and maximum value of Net Cost
     *
     * @param EntityProduct $product
     * @param Constraint $constraint
     */
    private function addNetCostValidation(EntityProduct $product, Constraint $constraint) : void
    {
        if ($product->getNetCost() < Product::COST_MIN) {
            $this->context->buildViolation($constraint->costMinMessage)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('{{ costMin }}', Product::COST_MIN)
                ->addViolation();
        }

        if ($product->getNetCost() > Product::COST_MAX) {
            $this->context->buildViolation($constraint->costMaxMessage)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('{{ costMax }}', Product::COST_MAX)
                ->addViolation();
        }
    }

    /**
     * @param EntityProduct $product
     * @param Constraint $constraint
     * @param ReflectionProperty $property
     */
    private function addPropertyNullValidation(
        EntityProduct $product,
        Constraint $constraint,
        ReflectionProperty $property
    ) {
        if ($property->getValue($product) === null) {
            $this->context->buildViolation($constraint->propertyNull)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('{{ property }}', $property->getName())
                ->addViolation();
        }
    }

    /**
     * @param EntityProduct $product
     * @param Constraint $constraint
     * @param ReflectionProperty $property
     */
    private function addPropertyEmptyValidation(
        EntityProduct $product,
        Constraint $constraint,
        ReflectionProperty $property
    ) {
        if ($property->getValue($product) === '') {
            $this->context->buildViolation($constraint->propertyEmpty)
                ->setParameter('{{ code }}', $product->getCode())
                ->setParameter('{{ property }}', $property->getName())
                ->addViolation();
        }
    }

    /**
     * Returns all the properties for the given Product object
     *
     * @param EntityProduct $product
     *
     * @return ReflectionProperty[]
     */
    private function getProperties(EntityProduct $product)
    {
        $reflect = new ReflectionClass($product);

        return $reflect->getProperties(ReflectionProperty::IS_PRIVATE);
    }
}
