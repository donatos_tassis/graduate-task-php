<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Serializer\Model\ProductData as ModelProductData;

class ProductDataValidator extends ConstraintValidator
{
    /**
     * @param ModelProductData $productData
     * @param Constraint $constraint
     */
    public function validate($productData, Constraint $constraint)
    {
        /* @var $constraint ProductData */

        if (null === $productData || '' === $productData) {
            return;
        }

        $data = $productData->getData();

        $this->addMissingKeyValidation($data, $constraint);
        $this->addExtraKeyValidation($data, $constraint);
        $this->addDiscontinuedChoicesValidation($data, $constraint);
    }

    /**
     * Adds validation to check that there are no missing keys in the input data
     *
     * @param array $data
     * @param Constraint $constraint
     */
    private function addMissingKeyValidation($data, $constraint)
    {
        $missingKeys = array_diff(ProductData::EXPECTED_KEYS, array_keys($data));
        foreach($missingKeys as $missingKey) {
            $this->context->buildViolation($constraint->missingKey)
                ->setParameter('{{ key }}', $missingKey)
                ->setParameter('{{ code }}', $this->getProductCode($data))
                ->addViolation();
        }
    }

    /**
     * Adds validation to check that there are no extra keys in the input data than expected
     *
     * @param array $data
     * @param Constraint $constraint
     */
    private function addExtraKeyValidation($data, $constraint)
    {
        $extraKeys = array_diff(array_keys($data), ProductData::EXPECTED_KEYS);
        foreach($extraKeys as $extraKey) {
            $this->context->buildViolation($constraint->extraKey)
                ->setParameter('{{ key }}', $extraKey)
                ->setParameter('{{ code }}', $this->getProductCode($data))
                ->addViolation();
        }
    }

    /**
     * Adds validation for key Discontinued to be sure that has one of the valid choices as value
     *
     * @param array $data
     * @param Constraint $constraint
     */
    private function addDiscontinuedChoicesValidation($data, $constraint)
    {
        if(isset($data['Discontinued']) && !in_array($data['Discontinued'], ProductData::DISCONTINUED_ACCEPTED_CHOICES)) {
            $this->context->buildViolation($constraint->wrongChoice)
                ->setParameter('{{ code }}', $this->getProductCode($data))
                ->addViolation();
        }
    }

    /**
     * @param array data
     *
     * @return string
     */
    private function getProductCode(array $data)
    {
        return $data['Product Code'] === '' ? 'Undefined' : $data['Product Code'];
    }
}
