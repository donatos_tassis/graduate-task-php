<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Product extends Constraint
{
    /** @const int */
    const STOCK_MIN = 10;

    /** @const float */
    const COST_MIN = 5;

    /** @const float */
    const COST_MAX = 1000;

    /** @const array */
    const EXCLUDED_KEYS = [
        'id',
        'code',
        'discontinued'
    ];

    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $stockMinMessage = '{{ code }} : This product cannot have Stock less than {{ stockMin }}.';
    public $costMinMessage = '{{ code }} : This product cannot have Net Cost less than {{ costMin }}.';
    public $costMaxMessage = '{{ code }} : This product cannot have Net Cost more than {{ costMax }}.';
    public $propertyNull = '{{ code }} : The property "{{ property }}" cannot be null.';
    public $propertyEmpty = '{{ code }} : The property "{{ property }}" cannot be empty.';
    public $productExists = '{{ code }} : This product already exists in the database.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
