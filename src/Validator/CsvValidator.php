<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\File as ConstraintFile;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class UploadedFileValidator
 *
 * @package App\Validator
 */
class CsvValidator extends AbstractFileValidator
{
    public function validate(File $file)
    {
        return $this->validator->validate(
            $file,
            [
                new ConstraintFile([
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'text/plain',
                        'text/csv',
                    ],
                    'mimeTypesMessage' => 'Only .csv files are accepted',
                    'maxSizeMessage' => 'File is too big',
                ]),
                new NotNull(['message'=>'Please select a file']),
                new NotBlank(['message'=>'File can not be empty'])
            ]
        );
    }
}
