<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractFileValidator
 *
 * @package App\Validator
 */
abstract class AbstractFileValidator implements FileValidatable
{
    /** @var ValidatorInterface $validator */
    protected $validator;

    /**
     * UploadedFileValidator constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
}