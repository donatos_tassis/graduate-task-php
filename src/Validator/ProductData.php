<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ProductData extends Constraint
{
    /** @const array */
    const EXPECTED_KEYS = [
        'Product Code',
        'Product Name',
        'Product Description',
        'Product Manufacture',
        'Stock',
        'Net Cost',
        'Tax Rate',
        'Discontinued'
    ];

    /**
     * The acceptable values that discontinued can take
     *
     * @const array
     */
    const DISCONTINUED_ACCEPTED_CHOICES = [
        'yes',
        'no',
        '',
        null
    ];

    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $missingKey = '{{ code }} : Missing key - {{ key }}';
    public $extraKey = '{{ code }} : Extra key provided - {{ key }}';
    public $wrongChoice = '{{ code }} : Invalid value for Discontinued. Please select yes/no or leave it empty';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
