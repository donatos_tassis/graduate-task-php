<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsViewController extends AbstractController
{
    /**
     * @Route("/products/view", name="products_view")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Paginator $paginator
     *
     * @return Response
     */
    public function view(Request $request, EntityManagerInterface $entityManager, Paginator $paginator)
    {
        $productsList = $entityManager->getRepository(Product::class)->findAll();
        $products = $paginator->paginate(
            $productsList,
            $request->query->getInt('page', 1), 10
        );

        return $this->render('products/products.view.html.twig', [
            'products' => $products,
        ]);
    }
}
