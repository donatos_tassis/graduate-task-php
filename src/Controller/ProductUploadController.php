<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ProductUploadType;
use App\Services\FileUploadHandler;
use App\Services\JsonResponseBuilder;
use App\Services\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductUploadController
 *
 * @package App\Controller
 */
class ProductUploadController extends AbstractController
{
    /**
     * Upload csv file controller
     *
     * @Route("/products/upload", name="products_upload")
     *
     * @param Request $request
     * @param FileUploadHandler $uploadHandler
     * @param Parser $parser
     *
     * @return JsonResponse|Response
     */
    public function upload(
        Request $request,
        FileUploadHandler $uploadHandler,
        Parser $parser
    ) {
        $form = $this->createForm(ProductUploadType::class, null, ['method' => 'POST']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /** @var File $file */
            $file = $request->files->get('file');

            $fileErrors = $uploadHandler->getValidationErrors($file);
            if(!$uploadHandler->isValidated()) {
                return new JsonResponseBuilder($fileErrors,400);
            }

            $contents = $uploadHandler->getContents($file);
            $data = $parser->parse($contents);

            return new JsonResponseBuilder($data,200);
        }

        return $this->render('products/product.upload.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
