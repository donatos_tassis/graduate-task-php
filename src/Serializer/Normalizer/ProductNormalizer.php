<?php

namespace App\Serializer\Normalizer;

use App\Entity\Product;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductNormalizer
 *
 * @package App\Serializer\Normalizer
 */
class ProductNormalizer implements DenormalizerInterface
{
    /** @var ObjectNormalizer $normalizer */
    private $normalizer;

    /** @var ValidatorInterface  $validator*/
    private $validator;

    /**
     * ProductNormalizer constructor.
     *
     * @param ObjectNormalizer $normalizer
     * @param ValidatorInterface $validator
     */
    public function __construct(ObjectNormalizer $normalizer, ValidatorInterface $validator)
    {
        $this->normalizer = $normalizer;
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, $type, string $format = null, array $context = [])
    {
        $product = new Product();

        $product->setCode($data['Product Code']);
        $product->setName($data['Product Name']);
        $product->setDescription($data['Product Description']);
        $product->setManufacture($data['Product Manufacture']);
        $product->setStock((int)$data['Stock']);
        $product->setNetCost((float)$data['Net Cost']);
        $product->setTaxRate((float)$data['Tax Rate']);
        $product->setDiscontinued($data['Discontinued']);

        $violations = $this->validator->validate($product);

        $errors = [];
        foreach ($violations as $violation) {
            $errors[] = $violation->getMessage();
        }

        if(count($violations) > 0) {
            return $errors;
        }

        return $product;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, string $format = null)
    {
        return Product::class === $type;
    }
}
