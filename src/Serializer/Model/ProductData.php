<?php


namespace App\Serializer\Model;

/**
 * Class ProductData
 *
 * @package App\Serializer\Model
 * @\App\Validator\ProductData()
 */
class ProductData
{
    /** @var array */
    private $data;

    /**
     * ProductData constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}