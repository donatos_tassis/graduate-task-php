<?php

declare(strict_types=1);

namespace App\Serializer\Model;

use App\Entity\Product;

/**
 * Class SucceedProductCollection
 *
 * @package App\Serializer\Model
 * @\App\Validator\UniqueProductCode()
 */
class SucceedProductCollection
{
    /**
     * @var Product[]
     */
    private $succeed;

    /**
     * SucceedProductCollection constructor.
     * @param Product[] $succeed
     */
    public function __construct(array $succeed)
    {
        $this->succeed = $succeed;
    }

    /**
     * @return Product[]
     */
    public function getSucceed(): array
    {
        return $this->succeed;
    }
}